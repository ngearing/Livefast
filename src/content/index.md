---
menu: /uploads/menu.pdf
about: >-
  Livefast Lifestyle Café was established not so long ago in October 2010, and
  has since been providing you with the best coffee, local produce and craft
  beer!!!


  We have a young and energetic team made up of artists, musicians, climbers and travelling creative types that love the beauty and nature of the Grampians.


  We love getting out in the National Park for a climb, run, ride or hike, and are never far away from a campfire and some live music.


  Our focus is on excellent coffee, and delicious food made with local ingredients wherever possible. Serving breakfast, lunch and other juicy items seven days a week, you will be sure to top up your tanks in the cafe by the creek!


  We aim to constantly evolve and we’re always on the search for new produce, people and places to explore!
---
