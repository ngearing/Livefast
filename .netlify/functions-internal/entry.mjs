import * as adapter from '@astrojs/netlify/netlify-functions.js';
import { renderers } from './renderers.mjs';
import 'mime';
import 'cookie';
import 'kleur/colors';
import { g as deserializeManifest } from './chunks/astro.d81e106a.mjs';
import '@astrojs/internal-helpers/path';
import 'path-to-regexp';
import 'string-width';
import 'html-escaper';

const _page0  = () => import('./chunks/admin-dashboard@_@astro.c2da949b.mjs');
const _page1  = () => import('./chunks/index@_@astro.63f2eb14.mjs');const pageMap = new Map([["node_modules/astro-netlify-cms/admin-dashboard.astro", _page0],["src/pages/index.astro", _page1]]);
const _manifest = Object.assign(deserializeManifest({"adapterName":"@astrojs/netlify/functions","routes":[{"file":"","links":[],"scripts":[{"type":"external","value":"/_astro/hoisted.d28502da.js"},{"type":"external","value":"/_astro/page.da31267c.js"}],"styles":[{"type":"external","src":"/_astro/admin-dashboard.559020a4.css"}],"routeData":{"type":"page","route":"/admin","pattern":"^\\/admin\\/?$","segments":[[{"content":"admin","dynamic":false,"spread":false}]],"params":[],"component":"node_modules/astro-netlify-cms/admin-dashboard.astro","pathname":"/admin","prerender":false,"_meta":{"trailingSlash":"ignore"}}},{"file":"","links":[],"scripts":[{"type":"external","value":"/_astro/page.da31267c.js"}],"styles":[{"type":"external","src":"/_astro/index.15f4c93a.css"}],"routeData":{"route":"/","type":"page","pattern":"^\\/$","segments":[],"params":[],"component":"src/pages/index.astro","pathname":"/","prerender":false,"_meta":{"trailingSlash":"ignore"}}}],"base":"/","compressHTML":false,"markdown":{"drafts":false,"syntaxHighlight":"shiki","shikiConfig":{"langs":[],"theme":"github-dark","wrap":false},"remarkPlugins":[],"rehypePlugins":[],"remarkRehype":{},"gfm":true,"smartypants":true},"componentMetadata":[["/Users/greengraphics/LocalSites/livefast.com.au/node_modules/astro-netlify-cms/admin-dashboard.astro",{"propagation":"none","containsHead":true}],["/Users/greengraphics/LocalSites/livefast.com.au/src/pages/index.astro",{"propagation":"none","containsHead":true}]],"renderers":[],"clientDirectives":[["idle","(()=>{var i=t=>{let e=async()=>{await(await t())()};\"requestIdleCallback\"in window?window.requestIdleCallback(e):setTimeout(e,200)};(self.Astro||(self.Astro={})).idle=i;window.dispatchEvent(new Event(\"astro:idle\"));})();"],["load","(()=>{var e=async t=>{await(await t())()};(self.Astro||(self.Astro={})).load=e;window.dispatchEvent(new Event(\"astro:load\"));})();"],["media","(()=>{var s=(i,t)=>{let a=async()=>{await(await i())()};if(t.value){let e=matchMedia(t.value);e.matches?a():e.addEventListener(\"change\",a,{once:!0})}};(self.Astro||(self.Astro={})).media=s;window.dispatchEvent(new Event(\"astro:media\"));})();"],["only","(()=>{var e=async t=>{await(await t())()};(self.Astro||(self.Astro={})).only=e;window.dispatchEvent(new Event(\"astro:only\"));})();"],["visible","(()=>{var r=(i,c,n)=>{let s=async()=>{await(await i())()},t=new IntersectionObserver(e=>{for(let o of e)if(o.isIntersecting){t.disconnect(),s();break}});for(let e of n.children)t.observe(e)};(self.Astro||(self.Astro={})).visible=r;window.dispatchEvent(new Event(\"astro:visible\"));})();"]],"entryModules":{"\u0000@astrojs-ssr-virtual-entry":"_@astrojs-ssr-virtual-entry.mjs","\u0000@astro-renderers":"renderers.mjs","\u0000empty-middleware":"_empty-middleware.mjs","/node_modules/astro-netlify-cms/admin-dashboard.astro":"chunks/pages/admin-dashboard.astro.e5e6ec26.mjs","/src/pages/index.astro":"chunks/pages/index.astro.cbd2c8c5.mjs","\u0000@astro-page:node_modules/astro-netlify-cms/admin-dashboard@_@astro":"chunks/admin-dashboard@_@astro.c2da949b.mjs","\u0000@astro-page:src/pages/index@_@astro":"chunks/index@_@astro.63f2eb14.mjs","/Users/greengraphics/LocalSites/livefast.com.au/src/content/index.md":"chunks/index.df58b82d.mjs","astro:scripts/page.js":"_astro/page.da31267c.js","/astro/hoisted.js?q=0":"_astro/hoisted.d28502da.js","astro:scripts/before-hydration.js":""},"assets":["/_astro/admin-dashboard.559020a4.css","/_astro/index.15f4c93a.css","/circle.svg","/facebook.svg","/favicon.png","/favicon.svg","/header.jpg","/instagram.svg","/tripadvisor.svg","/_astro/_commonjsHelpers.725317a4.js","/_astro/hoisted.d28502da.js","/_astro/page.da31267c.js","/uploads/menu.pdf","/_astro/page.da31267c.js"]}), {
	pageMap,
	renderers,
});
const _args = {};

const _exports = adapter.createExports(_manifest, _args);
const handler = _exports['handler'];

const _start = 'start';
if(_start in adapter) {
	adapter[_start](_manifest, _args);
}

export { handler, pageMap };
