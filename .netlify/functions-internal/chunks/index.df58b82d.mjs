import { h as createVNode, i as spreadAttributes, F as Fragment } from './astro.d81e106a.mjs';
import 'cookie';
import 'kleur/colors';
import '@astrojs/internal-helpers/path';
import 'path-to-regexp';
import 'mime';
import 'string-width';
import 'html-escaper';

const images = {
					
				};

				function updateImageReferences(html) {
					return html.replaceAll(
						/__ASTRO_IMAGE_="([^"]+)"/gm,
						(full, imagePath) => spreadAttributes({src: images[imagePath].src, ...images[imagePath].attributes})
					);
				}

				const html = updateImageReferences("");

				const frontmatter = {"menu":"/uploads/menu.pdf","about":"Livefast Lifestyle Café was established not so long ago in October 2010, and has since been providing you with the best coffee, local produce and craft beer!!!\n\nWe have a young and energetic team made up of artists, musicians, climbers and travelling creative types that love the beauty and nature of the Grampians.\n\nWe love getting out in the National Park for a climb, run, ride or hike, and are never far away from a campfire and some live music.\n\nOur focus is on excellent coffee, and delicious food made with local ingredients wherever possible. Serving breakfast, lunch and other juicy items seven days a week, you will be sure to top up your tanks in the cafe by the creek!\n\nWe aim to constantly evolve and we’re always on the search for new produce, people and places to explore!"};
				const file = "/Users/greengraphics/LocalSites/livefast.com.au/src/content/index.md";
				const url = undefined;
				function rawContent() {
					return "";
				}
				function compiledContent() {
					return html;
				}
				function getHeadings() {
					return [];
				}
				async function Content() {
					const { layout, ...content } = frontmatter;
					content.file = file;
					content.url = url;
					const contentFragment = createVNode(Fragment, { 'set:html': html });
					return contentFragment;
				}
				Content[Symbol.for('astro.needsHeadRendering')] = true;

export { Content, compiledContent, Content as default, file, frontmatter, getHeadings, images, rawContent, url };
