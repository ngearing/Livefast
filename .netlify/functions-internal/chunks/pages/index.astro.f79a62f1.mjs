import { c as createAstro, a as createComponent, r as renderTemplate, d as addAttribute, b as renderHead, e as renderSlot, f as renderComponent, m as maybeRenderHead, u as unescapeHTML } from '../astro.2fbecdff.mjs';
/* empty css                           */import { marked } from 'marked';
import 'cookie';
import 'kleur/colors';
import '@astrojs/internal-helpers/path';
import 'path-to-regexp';
import 'mime';
import 'string-width';
import 'html-escaper';

const $$Astro$1 = createAstro();
const $$Layout = createComponent(async ($$result, $$props, $$slots) => {
  const Astro2 = $$result.createAstro($$Astro$1, $$props, $$slots);
  Astro2.self = $$Layout;
  const { title } = Astro2.props;
  return renderTemplate`<html lang="en">
	<head>
		<title>${title}</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width">
		<meta name="description" content="Cafe">
		<meta name="generator"${addAttribute(Astro2.generator, "content")}>
		<link rel="icon" type="image/png" href="favicon.png">
		<link rel="preconnect" href="https://fonts.googleapis.com">
		<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
		<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Passion+One&family=Rubik:ital,wght@0,400;0,700;1,400;1,700&display=swap">
	${renderHead()}</head>
	<body>
		<header>
			<div class="site-brand">

				<h1 class="site-title">Live<span>fast</span></h1>
				<p class="site-description">Lifestyle cafe</p>

				<svg id="brand-circle" width="250" height="250" viewBox="0 0 250 250" fill="none" xmlns="http://www.w3.org/2000/svg">
					<path fill-rule="evenodd" clip-rule="evenodd" d="M250 125C250 144.736 245.426 163.403 237.281 180H142C136.477 180 132 184.477 132 190C132 195.523 136.477 200 142 200H225.009C202.204 230.361 165.895 250 125 250C55.9644 250 0 194.036 0 125C0 55.9644 55.9644 0 125 0C194.036 0 250 55.9644 250 125ZM112 200C117.523 200 122 195.523 122 190C122 184.477 117.523 180 112 180C106.477 180 102 184.477 102 190C102 195.523 106.477 200 112 200Z" fill="#5D9733"></path>
					<circle cx="52" cy="190" r="10" fill="#A5D596"></circle>
					<circle cx="82" cy="190" r="10" fill="#91BE58"></circle>
				</svg>
			</div>
		</header>

    	<main>
			${renderSlot($$result, $$slots["default"])}
		</main>

		<footer>
			<p>© 2023 <a href="https://www.livefast.com.au">Livefast Cafe</a> | <a href="http://www.greengraphics.com.au"><img src="https://www.livefast.com.au/wp-content/themes/ggstyle/images/gglogo.png"></a></p>
		</footer>
	</body></html>`;
}, "/Users/greengraphics/LocalSites/livefast.com.au/src/layouts/Layout.astro", void 0);

const $$Astro = createAstro();
const $$Index = createComponent(async ($$result, $$props, $$slots) => {
  const Astro2 = $$result.createAstro($$Astro, $$props, $$slots);
  Astro2.self = $$Index;
  let page = await Astro2.glob(/* #__PURE__ */ Object.assign({"../content/index.md": () => import('../index.7b108988.mjs')}), () => "../content/index.md");
  page = page.pop();
  page.frontmatter.about = marked.parse(page.frontmatter.about);
  return renderTemplate`${renderComponent($$result, "Layout", $$Layout, { "title": "Livefast" }, { "default": ($$result2) => renderTemplate`
    ${maybeRenderHead()}<article>
        <h2>Our Menu</h2>
        <p><a${addAttribute(page.frontmatter.menu, "href")} target="_blank">Download our latest menu here.</a></p>

        <h2>Contact</h2>

        <div class="cols">
            <div class="col">
                <h4>Livefast Lifestyle Cafe</h4>

                <p>
                    <strong>Phone:</strong> (03) 53 564 400<br>
                    <strong>Email:</strong> info@livefast.com.au
                </p>

                <p><strong>Address: </strong><br>
                    5/97 Grampians Road,<br>
                    Halls Gap, Victoria,<br> 3381
                </p>
            </div>
            <div class="col">
                <h4>Open Times</h4>

                <table>
                    <tbody>
                        <tr>
                            <td>Mon:</td>
                            <td>7am - 2pm</td>
                        </tr>
                        <tr>
                            <td>Tue:</td>
                            <td>7am - 2pm</td>
                        </tr>
                        <tr>
                            <td>Wed:</td>
                            <td>7am - 4pm</td>
                        </tr>
                        <tr>
                            <td>Thu:</td>
                            <td>7am - 4pm</td>
                        </tr>
                        <tr>
                            <td>Fri:</td>
                            <td>7am - 4pm</td>
                        </tr>
                        <tr>
                            <td>Sat:</td>
                            <td>7am - 5pm</td>
                        </tr>
                        <tr>
                            <td>Sun:</td>
                            <td>7am - 5pm</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3180.579752962429!2d142.517241475854!3d-37.138911972152464!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6ace0f8e031c3fff%3A0xce3a3d6304d306e4!2sLivefast%20Cafe!5e0!3m2!1sen!2sau!4v1691467619874!5m2!1sen!2sau" width="600" height="500" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>

    </article>

    <aside>
        <div class="follow">
            <h3>Follow us</h3>
            <p class="icons">
                <a href="https://www.facebook.com/pages/Livefast-Cafe/130004683714091" title="Facebook" target="_blank" rel="noopener">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                        <path d="M504 256C504 119 393 8 256 8S8 119 8 256c0 123.78 90.69 226.38 209.25 245V327.69h-63V256h63v-54.64c0-62.15 37-96.48 93.67-96.48 27.14 0 55.52 4.84 55.52 4.84v61h-31.28c-30.8 0-40.41 19.12-40.41 38.73V256h68.78l-11 71.69h-57.78V501C413.31 482.38 504 379.78 504 256z"></path>
                    </svg>
                </a>
                <a href="https://www.instagram.com/livefastcafe/" title="Instagram" target="_blank" rel="noopener">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                        <path d="M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z"></path>
                    </svg>
                </a>
                <a href="http://www.tripadvisor.com.au/Restaurant_Review-g552177-d2419628-Reviews-Livefast_Lifestyle_Cafe-Halls_Gap_Grampians_Victoria.html" title="Trip Advisor" target="_blank" rel="noopener">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                        <path d="M23.011 9.532c.281-1.207 1.175-2.416 1.175-2.416h-4.012c-2.251-1.455-4.981-2.226-8.013-2.226-3.14 0-5.978.78-8.214 2.251H.186s.885 1.186 1.17 2.386C.624 10.534.189 11.749.189 13.084c0 3.316 2.697 6.008 6.012 6.008 1.891 0 3.571-.885 4.681-2.254l1.275 1.916 1.291-1.936c.57.736 1.32 1.336 2.205 1.74 1.455.66 3.092.736 4.592.18 3.106-1.154 4.696-4.621 3.556-7.726-.209-.556-.48-1.051-.81-1.485l.02.005zm-3.171 8.072c-1.2.445-2.505.395-3.67-.143-.824-.383-1.503-.982-1.988-1.727-.201-.299-.375-.623-.503-.971-.146-.395-.22-.803-.259-1.215-.074-.832.045-1.673.405-2.453.54-1.164 1.501-2.051 2.701-2.496 2.49-.914 5.25.361 6.166 2.841.916 2.481-.36 5.245-2.835 6.163h-.017zm-9.668-1.834c-.863 1.271-2.322 2.113-3.973 2.113-2.646 0-4.801-2.156-4.801-4.797 0-2.641 2.156-4.802 4.801-4.802s4.798 2.161 4.798 4.802c0 .164-.03.314-.048.479-.081.811-.341 1.576-.777 2.221v-.016zM3.15 13.023c0 1.641 1.336 2.971 2.971 2.971s2.968-1.33 2.968-2.971c0-1.635-1.333-2.964-2.966-2.964-1.636 0-2.971 1.329-2.971 2.964H3.15zm12.048 0c0 1.641 1.329 2.971 2.968 2.971 1.636 0 2.965-1.33 2.965-2.971 0-1.635-1.329-2.964-2.965-2.964-1.635 0-2.971 1.329-2.971 2.964h.003zm-11.022 0c0-1.071.869-1.943 1.936-1.943 1.064 0 1.949.873 1.949 1.943 0 1.076-.869 1.951-1.949 1.951-1.081 0-1.951-.875-1.951-1.951h.015zm12.033 0c0-1.071.869-1.943 1.949-1.943 1.066 0 1.937.873 1.937 1.943 0 1.076-.87 1.951-1.952 1.951-1.079 0-1.949-.875-1.949-1.951h.015zM12.156 5.94c2.161 0 4.111.389 5.822 1.162-.645.018-1.275.131-1.906.36-1.515.555-2.715 1.665-3.375 3.125-.315.66-.48 1.359-.541 2.065-.225-3.076-2.76-5.515-5.881-5.578C7.986 6.34 9.967 5.94 12.112 5.94h.044z"></path>
                    </svg>
                </a>
            </p>
        </div>

        <h3>About us</h3>
 
        <div>${unescapeHTML(page.frontmatter.about)}</div>

    </aside>
` })}`;
}, "/Users/greengraphics/LocalSites/livefast.com.au/src/pages/index.astro", void 0);

const $$file = "/Users/greengraphics/LocalSites/livefast.com.au/src/pages/index.astro";
const $$url = "";

export { $$Index as default, $$file as file, $$url as url };
