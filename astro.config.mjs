import { defineConfig } from 'astro/config';
import NetlifyCMS from 'astro-netlify-cms';

import netlify from "@astrojs/netlify/functions";

// https://astro.build/config
export default defineConfig({
  integrations: [NetlifyCMS({
    config: {
      backend: {
        name: 'git-gateway',
        branch: 'master'
      },
      media_folder: '/public/uploads/',
      // Folder where user uploaded files should go to, relative to repo root (optional)
      public_folder: '/uploads/',
      collections: [{
        name: 'pages',
        label: "Pages",
        files: [{
          name: 'home',
          label: 'Home Page',
          file: 'src/content/index.md',
          fields: [{
            name: 'menu',
            label: 'Menu PDF',
            widget: 'file',
            default: "/public/uploads/menu.pdf"
          }, {
            name: 'about',
            label: 'About text',
            widget: 'markdown',
            buttons: ['bold', 'italic', 'bulleted-list', 'link', 'numbered-list', 'quote'],
            editor_components: []
          }]
        }]
      }]
    }
  })],
  output: "server",
  adapter: netlify()
});
